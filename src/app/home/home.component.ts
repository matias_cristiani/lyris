import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

import { WebView, LoadEventData } from "tns-core-modules/ui/web-view";

import { Page } from "tns-core-modules/ui/page";
import { TextField } from "tns-core-modules/ui/text-field";
import { Label } from "tns-core-modules/ui/label";

import { LoadingIndicator } from "nativescript-loading-indicator";
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

import * as ApplicationSettings from "tns-core-modules/application-settings";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as connectivity from "tns-core-modules/connectivity";


// optional options
var loader = new LoadingIndicator();

var options = {
  message: 'Cargando...',
  progress: 0.65,
  android: {
    indeterminate: true,
    cancelable: false,
    max: 100,
    progressNumberFormat: "%1d/%2d",
    progressPercentFormat: 0.53,
    progressStyle: 1,
    secondaryProgress: 1
  },
  ios: {
    details: "Conectando al servidor",
    margin: 10,
    dimBackground: true,
    color: "#4B9ED6", // color of indicator and labels
    backgroundColor: "yellow",
    hideBezel: true, // default false, can hide the surrounding bezel
  }
};



@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home-common.css", "./home.component.css"],
})

export class HomeComponent implements OnInit, AfterViewInit{
    
    webViewSrc: string = "https://empresas.librerialyris.com/mobile-app";
    @ViewChild("myWebView", {static: false}) webViewRef: ElementRef;

    visible: string = "hidden";
    

    constructor(private router: RouterExtensions, private changeDetectorRef: ChangeDetectorRef, private _page: Page){
           
           loader.show(options); 
           this._page.actionBarHidden = true;

           let connectionType = connectivity.getConnectionType();
            switch (connectionType) {
                case connectivity.connectionType.none:
                  this.router.navigate(["/sin-conexion"], { clearHistory: true }); 
                    console.log("None");
                    break;
                case connectivity.connectionType.wifi:
                    console.log("Wi-Fi");
                    break;
                case connectivity.connectionType.mobile:
                    console.log("Mobile");
                    break;
                default:
                    break;
            }

        


    }

    ngOnInit() {
        this._page.actionBarHidden = true;
        setTimeout(()=>{
            this.visible = 'visible';
        }, 2500)
    }
    
    ngAfterViewInit() {
        let webview: WebView = this.webViewRef.nativeElement;
  
        webview.on(WebView.loadFinishedEvent, function (args: LoadEventData) {
            loader.hide();
            let message;
            if (!args.error) {
                message = "WebView finished loading of " + args.url;

                if (isAndroid) {
                    webview.android.getSettings().setBuiltInZoomControls(false);
                }              
                           
            } else {
                message = "Error Cargando " + args.url + ": " + args.error;
            }

            console.dir("message - " + message);
        });
    }

    goBack() {
        let webview: WebView = this.webViewRef.nativeElement;
        if (webview.canGoBack) {
            webview.goBack();
        }
    }

    


 
}