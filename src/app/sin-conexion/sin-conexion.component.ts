import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import * as connectivity from "tns-core-modules/connectivity";
@Component({
    selector: "SinConexion",
    moduleId: module.id,
    templateUrl: "./sin-conexion.component.html",
    styleUrls: ["./sin-conexion-common.css", "./sin-conexion.component.css"],
})

export class SinConexionComponent implements OnInit, AfterViewInit{
    
    constructor(private router: RouterExtensions, private changeDetectorRef: ChangeDetectorRef){
           
    }

    ngOnInit() {
    }
    
    ngAfterViewInit() {

    }

    onButtonTap() {


           let connectionType = connectivity.getConnectionType();
            
            switch (connectionType) {
                case connectivity.connectionType.none:
                        
                        let options = {
                            title: "SIN INTERNET",
                            message: "Aún no posee una conexión estable a internet.",
                            okButtonText: "OK"
                        };

                        alert(options)

                    console.log("None");
                    break;
                case connectivity.connectionType.wifi:
                    this.router.navigate(["/home"], { clearHistory: true }); 
                    console.log("Wi-Fi");
                    break;
                case connectivity.connectionType.mobile:
                    this.router.navigate(["/home"], { clearHistory: true }); 
                    console.log("Mobile");
                    break;
                default:
                    break;
            }

    }

}