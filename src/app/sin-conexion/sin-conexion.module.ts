import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SinConexionRoutingModule } from "./sin-conexion-routing.module";
import { SinConexionComponent } from "./sin-conexion.component";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        SinConexionRoutingModule
    ],
    declarations: [
        SinConexionComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SinConexionModule { }
