import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { SinConexionComponent } from "./sin-conexion.component";

const routes: Routes = [
    { path: "", component: SinConexionComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SinConexionRoutingModule { }
